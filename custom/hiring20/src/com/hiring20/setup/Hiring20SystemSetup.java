/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hiring20.setup;

import static com.hiring20.constants.Hiring20Constants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.hiring20.constants.Hiring20Constants;
import com.hiring20.service.Hiring20Service;


@SystemSetup(extension = Hiring20Constants.EXTENSIONNAME)
public class Hiring20SystemSetup
{
	private final Hiring20Service hiring20Service;

	public Hiring20SystemSetup(final Hiring20Service hiring20Service)
	{
		this.hiring20Service = hiring20Service;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		hiring20Service.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return Hiring20SystemSetup.class.getResourceAsStream("/hiring20/sap-hybris-platform.png");
	}
}
