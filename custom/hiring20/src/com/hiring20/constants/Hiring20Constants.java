/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hiring20.constants;

/**
 * Global class for all Hiring20 constants. You can add global constants for your extension into this class.
 */
public final class Hiring20Constants extends GeneratedHiring20Constants
{
	public static final String EXTENSIONNAME = "hiring20";

	private Hiring20Constants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

    public static final String PLATFORM_LOGO_CODE = "hiring20PlatformLogo";
}
