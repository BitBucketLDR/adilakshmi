/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 */
package com.hiring20.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedHiring20Constants
{
	public static final String EXTENSIONNAME = "hiring20";
	public static class TC
	{
		public static final String COURSE = "Course".intern();
		public static final String LIBRARYCARD = "LibraryCard".intern();
		public static final String STUDENT = "Student".intern();
		public static final String STUDENTTYPE = "StudentType".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	public static class Enumerations
	{
		public static class StudentType
		{
			public static final String REGULAR = "REGULAR".intern();
			public static final String DISTANCE = "DISTANCE".intern();
		}
	}
	
	protected GeneratedHiring20Constants()
	{
		// private constructor
	}
	
	
}
