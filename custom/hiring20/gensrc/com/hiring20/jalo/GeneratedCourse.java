/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 */
package com.hiring20.jalo;

import com.hiring20.constants.Hiring20Constants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.jalo.GenericItem Course}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedCourse extends GenericItem
{
	/** Qualifier of the <code>Course.subcourse</code> attribute **/
	public static final String SUBCOURSE = "subcourse";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(SUBCOURSE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Course.subcourse</code> attribute.
	 * @return the subcourse
	 */
	public String getSubcourse(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SUBCOURSE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Course.subcourse</code> attribute.
	 * @return the subcourse
	 */
	public String getSubcourse()
	{
		return getSubcourse( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Course.subcourse</code> attribute. 
	 * @param value the subcourse
	 */
	public void setSubcourse(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SUBCOURSE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Course.subcourse</code> attribute. 
	 * @param value the subcourse
	 */
	public void setSubcourse(final String value)
	{
		setSubcourse( getSession().getSessionContext(), value );
	}
	
}
