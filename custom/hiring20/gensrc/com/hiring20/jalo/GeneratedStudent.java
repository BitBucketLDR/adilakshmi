/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 */
package com.hiring20.jalo;

import com.hiring20.constants.Hiring20Constants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Generated class for type {@link de.hybris.platform.jalo.GenericItem Student}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedStudent extends GenericItem
{
	/** Qualifier of the <code>Student.name</code> attribute **/
	public static final String NAME = "name";
	/** Qualifier of the <code>Student.id</code> attribute **/
	public static final String ID = "id";
	/** Qualifier of the <code>Student.city</code> attribute **/
	public static final String CITY = "city";
	/** Qualifier of the <code>Student.course</code> attribute **/
	public static final String COURSE = "course";
	/** Qualifier of the <code>Student.fname</code> attribute **/
	public static final String FNAME = "fname";
	/** Qualifier of the <code>Student.lname</code> attribute **/
	public static final String LNAME = "lname";
	/** Qualifier of the <code>Student.email</code> attribute **/
	public static final String EMAIL = "email";
	/** Qualifier of the <code>Student.studentType</code> attribute **/
	public static final String STUDENTTYPE = "studentType";
	/** Qualifier of the <code>Student.hobbies</code> attribute **/
	public static final String HOBBIES = "hobbies";
	/** Qualifier of the <code>Student.feedBack</code> attribute **/
	public static final String FEEDBACK = "feedBack";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(NAME, AttributeMode.INITIAL);
		tmp.put(ID, AttributeMode.INITIAL);
		tmp.put(CITY, AttributeMode.INITIAL);
		tmp.put(COURSE, AttributeMode.INITIAL);
		tmp.put(FNAME, AttributeMode.INITIAL);
		tmp.put(LNAME, AttributeMode.INITIAL);
		tmp.put(EMAIL, AttributeMode.INITIAL);
		tmp.put(STUDENTTYPE, AttributeMode.INITIAL);
		tmp.put(HOBBIES, AttributeMode.INITIAL);
		tmp.put(FEEDBACK, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.city</code> attribute.
	 * @return the city
	 */
	public String getCity(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.city</code> attribute.
	 * @return the city
	 */
	public String getCity()
	{
		return getCity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.city</code> attribute. 
	 * @param value the city
	 */
	public void setCity(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.city</code> attribute. 
	 * @param value the city
	 */
	public void setCity(final String value)
	{
		setCity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.course</code> attribute.
	 * @return the course
	 */
	public String getCourse(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COURSE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.course</code> attribute.
	 * @return the course
	 */
	public String getCourse()
	{
		return getCourse( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.course</code> attribute. 
	 * @param value the course
	 */
	public void setCourse(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COURSE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.course</code> attribute. 
	 * @param value the course
	 */
	public void setCourse(final String value)
	{
		setCourse( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.email</code> attribute.
	 * @return the email
	 */
	public String getEmail(final SessionContext ctx)
	{
		return (String)getProperty( ctx, EMAIL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.email</code> attribute.
	 * @return the email
	 */
	public String getEmail()
	{
		return getEmail( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.email</code> attribute. 
	 * @param value the email
	 */
	public void setEmail(final SessionContext ctx, final String value)
	{
		setProperty(ctx, EMAIL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.email</code> attribute. 
	 * @param value the email
	 */
	public void setEmail(final String value)
	{
		setEmail( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.feedBack</code> attribute.
	 * @return the feedBack
	 */
	public Map<Date,String> getAllFeedBack(final SessionContext ctx)
	{
		Map<Date,String> map = (Map<Date,String>)getProperty( ctx, FEEDBACK);
		return map != null ? map : Collections.EMPTY_MAP;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.feedBack</code> attribute.
	 * @return the feedBack
	 */
	public Map<Date,String> getAllFeedBack()
	{
		return getAllFeedBack( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.feedBack</code> attribute. 
	 * @param value the feedBack
	 */
	public void setAllFeedBack(final SessionContext ctx, final Map<Date,String> value)
	{
		setProperty(ctx, FEEDBACK,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.feedBack</code> attribute. 
	 * @param value the feedBack
	 */
	public void setAllFeedBack(final Map<Date,String> value)
	{
		setAllFeedBack( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.fname</code> attribute.
	 * @return the fname
	 */
	public String getFname(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.fname</code> attribute.
	 * @return the fname
	 */
	public String getFname()
	{
		return getFname( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.fname</code> attribute. 
	 * @param value the fname
	 */
	public void setFname(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.fname</code> attribute. 
	 * @param value the fname
	 */
	public void setFname(final String value)
	{
		setFname( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.hobbies</code> attribute.
	 * @return the hobbies
	 */
	public Set<String> getHobbies(final SessionContext ctx)
	{
		Set<String> coll = (Set<String>)getProperty( ctx, HOBBIES);
		return coll != null ? coll : Collections.EMPTY_SET;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.hobbies</code> attribute.
	 * @return the hobbies
	 */
	public Set<String> getHobbies()
	{
		return getHobbies( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.hobbies</code> attribute. 
	 * @param value the hobbies
	 */
	public void setHobbies(final SessionContext ctx, final Set<String> value)
	{
		setProperty(ctx, HOBBIES,value == null || !value.isEmpty() ? value : null );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.hobbies</code> attribute. 
	 * @param value the hobbies
	 */
	public void setHobbies(final Set<String> value)
	{
		setHobbies( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.id</code> attribute.
	 * @return the id
	 */
	public String getId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.id</code> attribute.
	 * @return the id
	 */
	public String getId()
	{
		return getId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.id</code> attribute. 
	 * @param value the id
	 */
	public void setId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.id</code> attribute. 
	 * @param value the id
	 */
	public void setId(final String value)
	{
		setId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.lname</code> attribute.
	 * @return the lname
	 */
	public String getLname(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.lname</code> attribute.
	 * @return the lname
	 */
	public String getLname()
	{
		return getLname( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.lname</code> attribute. 
	 * @param value the lname
	 */
	public void setLname(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.lname</code> attribute. 
	 * @param value the lname
	 */
	public void setLname(final String value)
	{
		setLname( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.name</code> attribute.
	 * @return the name
	 */
	public String getName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, NAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.name</code> attribute.
	 * @return the name
	 */
	public String getName()
	{
		return getName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.name</code> attribute. 
	 * @param value the name
	 */
	public void setName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, NAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.name</code> attribute. 
	 * @param value the name
	 */
	public void setName(final String value)
	{
		setName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.studentType</code> attribute.
	 * @return the studentType
	 */
	public EnumerationValue getStudentType(final SessionContext ctx)
	{
		return (EnumerationValue)getProperty( ctx, STUDENTTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Student.studentType</code> attribute.
	 * @return the studentType
	 */
	public EnumerationValue getStudentType()
	{
		return getStudentType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.studentType</code> attribute. 
	 * @param value the studentType
	 */
	public void setStudentType(final SessionContext ctx, final EnumerationValue value)
	{
		setProperty(ctx, STUDENTTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Student.studentType</code> attribute. 
	 * @param value the studentType
	 */
	public void setStudentType(final EnumerationValue value)
	{
		setStudentType( getSession().getSessionContext(), value );
	}
	
}
