/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 */
package com.hiring20.jalo;

import com.hiring20.constants.Hiring20Constants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.jalo.GenericItem LibraryCard}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedLibraryCard extends GenericItem
{
	/** Qualifier of the <code>LibraryCard.uid</code> attribute **/
	public static final String UID = "uid";
	/** Qualifier of the <code>LibraryCard.dateofIssue</code> attribute **/
	public static final String DATEOFISSUE = "dateofIssue";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(UID, AttributeMode.INITIAL);
		tmp.put(DATEOFISSUE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>LibraryCard.dateofIssue</code> attribute.
	 * @return the dateofIssue
	 */
	public Date getDateofIssue(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, DATEOFISSUE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>LibraryCard.dateofIssue</code> attribute.
	 * @return the dateofIssue
	 */
	public Date getDateofIssue()
	{
		return getDateofIssue( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>LibraryCard.dateofIssue</code> attribute. 
	 * @param value the dateofIssue
	 */
	public void setDateofIssue(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, DATEOFISSUE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>LibraryCard.dateofIssue</code> attribute. 
	 * @param value the dateofIssue
	 */
	public void setDateofIssue(final Date value)
	{
		setDateofIssue( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>LibraryCard.uid</code> attribute.
	 * @return the uid
	 */
	public String getUid(final SessionContext ctx)
	{
		return (String)getProperty( ctx, UID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>LibraryCard.uid</code> attribute.
	 * @return the uid
	 */
	public String getUid()
	{
		return getUid( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>LibraryCard.uid</code> attribute. 
	 * @param value the uid
	 */
	public void setUid(final SessionContext ctx, final String value)
	{
		setProperty(ctx, UID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>LibraryCard.uid</code> attribute. 
	 * @param value the uid
	 */
	public void setUid(final String value)
	{
		setUid( getSession().getSessionContext(), value );
	}
	
}
