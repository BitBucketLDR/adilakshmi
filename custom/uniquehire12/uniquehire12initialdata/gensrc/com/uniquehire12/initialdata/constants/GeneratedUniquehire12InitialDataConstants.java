/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 */
package com.uniquehire12.initialdata.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedUniquehire12InitialDataConstants
{
	public static final String EXTENSIONNAME = "uniquehire12initialdata";
	
	protected GeneratedUniquehire12InitialDataConstants()
	{
		// private constructor
	}
	
	
}
