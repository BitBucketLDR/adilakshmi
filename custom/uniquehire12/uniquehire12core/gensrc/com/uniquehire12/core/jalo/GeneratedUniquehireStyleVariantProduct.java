/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 */
package com.uniquehire12.core.jalo;

import com.uniquehire12.core.constants.Uniquehire12CoreConstants;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.variants.jalo.VariantProduct;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.uniquehire12.core.jalo.UniquehireStyleVariantProduct UniquehireStyleVariantProduct}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedUniquehireStyleVariantProduct extends VariantProduct
{
	/** Qualifier of the <code>UniquehireStyleVariantProduct.uniquetype</code> attribute **/
	public static final String UNIQUETYPE = "uniquetype";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(VariantProduct.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(UNIQUETYPE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UniquehireStyleVariantProduct.uniquetype</code> attribute.
	 * @return the uniquetype
	 */
	public String getUniquetype(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedUniquehireStyleVariantProduct.getUniquetype requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, UNIQUETYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UniquehireStyleVariantProduct.uniquetype</code> attribute.
	 * @return the uniquetype
	 */
	public String getUniquetype()
	{
		return getUniquetype( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UniquehireStyleVariantProduct.uniquetype</code> attribute. 
	 * @return the localized uniquetype
	 */
	public Map<Language,String> getAllUniquetype(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,UNIQUETYPE,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UniquehireStyleVariantProduct.uniquetype</code> attribute. 
	 * @return the localized uniquetype
	 */
	public Map<Language,String> getAllUniquetype()
	{
		return getAllUniquetype( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UniquehireStyleVariantProduct.uniquetype</code> attribute. 
	 * @param value the uniquetype
	 */
	public void setUniquetype(final SessionContext ctx, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedUniquehireStyleVariantProduct.setUniquetype requires a session language", 0 );
		}
		setLocalizedProperty(ctx, UNIQUETYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UniquehireStyleVariantProduct.uniquetype</code> attribute. 
	 * @param value the uniquetype
	 */
	public void setUniquetype(final String value)
	{
		setUniquetype( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UniquehireStyleVariantProduct.uniquetype</code> attribute. 
	 * @param value the uniquetype
	 */
	public void setAllUniquetype(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,UNIQUETYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UniquehireStyleVariantProduct.uniquetype</code> attribute. 
	 * @param value the uniquetype
	 */
	public void setAllUniquetype(final Map<Language,String> value)
	{
		setAllUniquetype( getSession().getSessionContext(), value );
	}
	
}
