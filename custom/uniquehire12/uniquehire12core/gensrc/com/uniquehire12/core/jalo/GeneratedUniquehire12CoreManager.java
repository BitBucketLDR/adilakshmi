/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 */
package com.uniquehire12.core.jalo;

import com.uniquehire12.core.constants.Uniquehire12CoreConstants;
import com.uniquehire12.core.jalo.ApparelProduct;
import com.uniquehire12.core.jalo.ApparelSizeVariantProduct;
import com.uniquehire12.core.jalo.ApparelStyleVariantProduct;
import com.uniquehire12.core.jalo.Brand;
import com.uniquehire12.core.jalo.ElectronicsColorVariantProduct;
import com.uniquehire12.core.jalo.HelloWorldCron;
import com.uniquehire12.core.jalo.PradeepRegistrationProcess;
import com.uniquehire12.core.jalo.RegistrationProcess;
import com.uniquehire12.core.jalo.UniquehireStyleVariantProduct;
import de.hybris.platform.cms2.jalo.contents.components.ThejuGorantlaComponent;
import de.hybris.platform.cms2.jalo.contents.components.UniqueHireComponent;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.jalo.user.Customer;
import de.hybris.platform.jalo.user.User;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type <code>Uniquehire12CoreManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedUniquehire12CoreManager extends Extension
{
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put("phonenumber", AttributeMode.INITIAL);
		tmp.put("Age", AttributeMode.INITIAL);
		tmp.put("DateOfBirth", AttributeMode.INITIAL);
		tmp.put("DOB", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.user.Customer", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("rental", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.order.AbstractOrderEntry", Collections.unmodifiableMap(tmp));
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.Age</code> attribute.
	 * @return the Age
	 */
	public String getAge(final SessionContext ctx, final Customer item)
	{
		return (String)item.getProperty( ctx, Uniquehire12CoreConstants.Attributes.Customer.AGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.Age</code> attribute.
	 * @return the Age
	 */
	public String getAge(final Customer item)
	{
		return getAge( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.Age</code> attribute. 
	 * @param value the Age
	 */
	public void setAge(final SessionContext ctx, final Customer item, final String value)
	{
		item.setProperty(ctx, Uniquehire12CoreConstants.Attributes.Customer.AGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.Age</code> attribute. 
	 * @param value the Age
	 */
	public void setAge(final Customer item, final String value)
	{
		setAge( getSession().getSessionContext(), item, value );
	}
	
	public ApparelProduct createApparelProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( Uniquehire12CoreConstants.TC.APPARELPRODUCT );
			return (ApparelProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelProduct createApparelProduct(final Map attributeValues)
	{
		return createApparelProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public ApparelSizeVariantProduct createApparelSizeVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( Uniquehire12CoreConstants.TC.APPARELSIZEVARIANTPRODUCT );
			return (ApparelSizeVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelSizeVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelSizeVariantProduct createApparelSizeVariantProduct(final Map attributeValues)
	{
		return createApparelSizeVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public ApparelStyleVariantProduct createApparelStyleVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( Uniquehire12CoreConstants.TC.APPARELSTYLEVARIANTPRODUCT );
			return (ApparelStyleVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelStyleVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelStyleVariantProduct createApparelStyleVariantProduct(final Map attributeValues)
	{
		return createApparelStyleVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public Brand createBrand(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( Uniquehire12CoreConstants.TC.BRAND );
			return (Brand)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating Brand : "+e.getMessage(), 0 );
		}
	}
	
	public Brand createBrand(final Map attributeValues)
	{
		return createBrand( getSession().getSessionContext(), attributeValues );
	}
	
	public ElectronicsColorVariantProduct createElectronicsColorVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( Uniquehire12CoreConstants.TC.ELECTRONICSCOLORVARIANTPRODUCT );
			return (ElectronicsColorVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ElectronicsColorVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ElectronicsColorVariantProduct createElectronicsColorVariantProduct(final Map attributeValues)
	{
		return createElectronicsColorVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public HelloWorldCron createHelloWorldCron(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( Uniquehire12CoreConstants.TC.HELLOWORLDCRON );
			return (HelloWorldCron)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating HelloWorldCron : "+e.getMessage(), 0 );
		}
	}
	
	public HelloWorldCron createHelloWorldCron(final Map attributeValues)
	{
		return createHelloWorldCron( getSession().getSessionContext(), attributeValues );
	}
	
	public PradeepRegistrationProcess createPradeepRegistrationProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( Uniquehire12CoreConstants.TC.PRADEEPREGISTRATIONPROCESS );
			return (PradeepRegistrationProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating PradeepRegistrationProcess : "+e.getMessage(), 0 );
		}
	}
	
	public PradeepRegistrationProcess createPradeepRegistrationProcess(final Map attributeValues)
	{
		return createPradeepRegistrationProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public RegistrationProcess createRegistrationProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( Uniquehire12CoreConstants.TC.REGISTRATIONPROCESS );
			return (RegistrationProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating RegistrationProcess : "+e.getMessage(), 0 );
		}
	}
	
	public RegistrationProcess createRegistrationProcess(final Map attributeValues)
	{
		return createRegistrationProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public ThejuGorantlaComponent createThejuGorantlaComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( Uniquehire12CoreConstants.TC.THEJUGORANTLACOMPONENT );
			return (ThejuGorantlaComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ThejuGorantlaComponent : "+e.getMessage(), 0 );
		}
	}
	
	public ThejuGorantlaComponent createThejuGorantlaComponent(final Map attributeValues)
	{
		return createThejuGorantlaComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public UniqueHireComponent createUniqueHireComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( Uniquehire12CoreConstants.TC.UNIQUEHIRECOMPONENT );
			return (UniqueHireComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating UniqueHireComponent : "+e.getMessage(), 0 );
		}
	}
	
	public UniqueHireComponent createUniqueHireComponent(final Map attributeValues)
	{
		return createUniqueHireComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public UniquehireStyleVariantProduct createUniquehireStyleVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( Uniquehire12CoreConstants.TC.UNIQUEHIRESTYLEVARIANTPRODUCT );
			return (UniquehireStyleVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating UniquehireStyleVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public UniquehireStyleVariantProduct createUniquehireStyleVariantProduct(final Map attributeValues)
	{
		return createUniquehireStyleVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.DateOfBirth</code> attribute.
	 * @return the DateOfBirth
	 */
	public String getDateOfBirth(final SessionContext ctx, final Customer item)
	{
		return (String)item.getProperty( ctx, Uniquehire12CoreConstants.Attributes.Customer.DATEOFBIRTH);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.DateOfBirth</code> attribute.
	 * @return the DateOfBirth
	 */
	public String getDateOfBirth(final Customer item)
	{
		return getDateOfBirth( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.DateOfBirth</code> attribute. 
	 * @param value the DateOfBirth
	 */
	public void setDateOfBirth(final SessionContext ctx, final Customer item, final String value)
	{
		item.setProperty(ctx, Uniquehire12CoreConstants.Attributes.Customer.DATEOFBIRTH,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.DateOfBirth</code> attribute. 
	 * @param value the DateOfBirth
	 */
	public void setDateOfBirth(final Customer item, final String value)
	{
		setDateOfBirth( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.DOB</code> attribute.
	 * @return the DOB
	 */
	public Date getDOB(final SessionContext ctx, final Customer item)
	{
		return (Date)item.getProperty( ctx, Uniquehire12CoreConstants.Attributes.Customer.DOB);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.DOB</code> attribute.
	 * @return the DOB
	 */
	public Date getDOB(final Customer item)
	{
		return getDOB( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.DOB</code> attribute. 
	 * @param value the DOB
	 */
	public void setDOB(final SessionContext ctx, final Customer item, final Date value)
	{
		item.setProperty(ctx, Uniquehire12CoreConstants.Attributes.Customer.DOB,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.DOB</code> attribute. 
	 * @param value the DOB
	 */
	public void setDOB(final Customer item, final Date value)
	{
		setDOB( getSession().getSessionContext(), item, value );
	}
	
	@Override
	public String getName()
	{
		return Uniquehire12CoreConstants.EXTENSIONNAME;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.phonenumber</code> attribute.
	 * @return the phonenumber
	 */
	public String getPhonenumber(final SessionContext ctx, final Customer item)
	{
		return (String)item.getProperty( ctx, Uniquehire12CoreConstants.Attributes.Customer.PHONENUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.phonenumber</code> attribute.
	 * @return the phonenumber
	 */
	public String getPhonenumber(final Customer item)
	{
		return getPhonenumber( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.phonenumber</code> attribute. 
	 * @param value the phonenumber
	 */
	public void setPhonenumber(final SessionContext ctx, final Customer item, final String value)
	{
		item.setProperty(ctx, Uniquehire12CoreConstants.Attributes.Customer.PHONENUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.phonenumber</code> attribute. 
	 * @param value the phonenumber
	 */
	public void setPhonenumber(final Customer item, final String value)
	{
		setPhonenumber( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.rental</code> attribute.
	 * @return the rental
	 */
	public String getRental(final SessionContext ctx, final AbstractOrderEntry item)
	{
		return (String)item.getProperty( ctx, Uniquehire12CoreConstants.Attributes.AbstractOrderEntry.RENTAL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.rental</code> attribute.
	 * @return the rental
	 */
	public String getRental(final AbstractOrderEntry item)
	{
		return getRental( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.rental</code> attribute. 
	 * @param value the rental
	 */
	public void setRental(final SessionContext ctx, final AbstractOrderEntry item, final String value)
	{
		item.setProperty(ctx, Uniquehire12CoreConstants.Attributes.AbstractOrderEntry.RENTAL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.rental</code> attribute. 
	 * @param value the rental
	 */
	public void setRental(final AbstractOrderEntry item, final String value)
	{
		setRental( getSession().getSessionContext(), item, value );
	}
	
}
