/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 */
package com.uniquehire12.core.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedUniquehire12CoreConstants
{
	public static final String EXTENSIONNAME = "uniquehire12core";
	public static class TC
	{
		public static final String APPARELPRODUCT = "ApparelProduct".intern();
		public static final String APPARELSIZEVARIANTPRODUCT = "ApparelSizeVariantProduct".intern();
		public static final String APPARELSTYLEVARIANTPRODUCT = "ApparelStyleVariantProduct".intern();
		public static final String BRAND = "Brand".intern();
		public static final String ELECTRONICSCOLORVARIANTPRODUCT = "ElectronicsColorVariantProduct".intern();
		public static final String HELLOWORLDCRON = "HelloWorldCron".intern();
		public static final String PRADEEPREGISTRATIONPROCESS = "PradeepRegistrationProcess".intern();
		public static final String REGISTRATIONPROCESS = "RegistrationProcess".intern();
		public static final String SWATCHCOLORENUM = "SwatchColorEnum".intern();
		public static final String THEJUGORANTLACOMPONENT = "ThejuGorantlaComponent".intern();
		public static final String UNIQUEHIRECOMPONENT = "UniqueHireComponent".intern();
		public static final String UNIQUEHIRESTYLEVARIANTPRODUCT = "UniquehireStyleVariantProduct".intern();
	}
	public static class Attributes
	{
		public static class AbstractOrderEntry
		{
			public static final String RENTAL = "rental".intern();
		}
		public static class Customer
		{
			public static final String AGE = "Age".intern();
			public static final String AGE1 = "Age1".intern();
			public static final String DATEOFBIRTH = "DateOfBirth".intern();
			public static final String DOB = "DOB".intern();
			public static final String PHONENUMBER = "phonenumber".intern();
		}
	}
	public static class Enumerations
	{
		public static class SwatchColorEnum
		{
			public static final String BLACK = "BLACK".intern();
			public static final String BLUE = "BLUE".intern();
			public static final String BROWN = "BROWN".intern();
			public static final String GREEN = "GREEN".intern();
			public static final String GREY = "GREY".intern();
			public static final String ORANGE = "ORANGE".intern();
			public static final String PINK = "PINK".intern();
			public static final String PURPLE = "PURPLE".intern();
			public static final String RED = "RED".intern();
			public static final String SILVER = "SILVER".intern();
			public static final String WHITE = "WHITE".intern();
			public static final String YELLOW = "YELLOW".intern();
		}
	}
	
	protected GeneratedUniquehire12CoreConstants()
	{
		// private constructor
	}
	
	
}
