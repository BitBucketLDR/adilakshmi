/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 *  
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *  
 */
package de.hybris.platform.cms2.jalo.contents.components;

import com.uniquehire12.core.constants.Uniquehire12CoreConstants;
import de.hybris.platform.cms2.jalo.contents.components.SimpleCMSComponent;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.jalo.media.Media;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.cms2.jalo.contents.components.ThejuGorantlaComponent ThejuGorantlaComponent}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedThejuGorantlaComponent extends SimpleCMSComponent
{
	/** Qualifier of the <code>ThejuGorantlaComponent.content</code> attribute **/
	public static final String CONTENT = "content";
	/** Qualifier of the <code>ThejuGorantlaComponent.image</code> attribute **/
	public static final String IMAGE = "image";
	/** Qualifier of the <code>ThejuGorantlaComponent.imageUrl</code> attribute **/
	public static final String IMAGEURL = "imageUrl";
	/** Qualifier of the <code>ThejuGorantlaComponent.linkUrl</code> attribute **/
	public static final String LINKURL = "linkUrl";
	/** Qualifier of the <code>ThejuGorantlaComponent.linkTitle</code> attribute **/
	public static final String LINKTITLE = "linkTitle";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(SimpleCMSComponent.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(CONTENT, AttributeMode.INITIAL);
		tmp.put(IMAGE, AttributeMode.INITIAL);
		tmp.put(IMAGEURL, AttributeMode.INITIAL);
		tmp.put(LINKURL, AttributeMode.INITIAL);
		tmp.put(LINKTITLE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.content</code> attribute.
	 * @return the content
	 */
	public String getContent(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedThejuGorantlaComponent.getContent requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, CONTENT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.content</code> attribute.
	 * @return the content
	 */
	public String getContent()
	{
		return getContent( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.content</code> attribute. 
	 * @return the localized content
	 */
	public Map<Language,String> getAllContent(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,CONTENT,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.content</code> attribute. 
	 * @return the localized content
	 */
	public Map<Language,String> getAllContent()
	{
		return getAllContent( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.content</code> attribute. 
	 * @param value the content
	 */
	public void setContent(final SessionContext ctx, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedThejuGorantlaComponent.setContent requires a session language", 0 );
		}
		setLocalizedProperty(ctx, CONTENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.content</code> attribute. 
	 * @param value the content
	 */
	public void setContent(final String value)
	{
		setContent( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.content</code> attribute. 
	 * @param value the content
	 */
	public void setAllContent(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,CONTENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.content</code> attribute. 
	 * @param value the content
	 */
	public void setAllContent(final Map<Language,String> value)
	{
		setAllContent( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.image</code> attribute.
	 * @return the image
	 */
	public Media getImage(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedThejuGorantlaComponent.getImage requires a session language", 0 );
		}
		return (Media)getLocalizedProperty( ctx, IMAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.image</code> attribute.
	 * @return the image
	 */
	public Media getImage()
	{
		return getImage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.image</code> attribute. 
	 * @return the localized image
	 */
	public Map<Language,Media> getAllImage(final SessionContext ctx)
	{
		return (Map<Language,Media>)getAllLocalizedProperties(ctx,IMAGE,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.image</code> attribute. 
	 * @return the localized image
	 */
	public Map<Language,Media> getAllImage()
	{
		return getAllImage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.image</code> attribute. 
	 * @param value the image
	 */
	public void setImage(final SessionContext ctx, final Media value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedThejuGorantlaComponent.setImage requires a session language", 0 );
		}
		setLocalizedProperty(ctx, IMAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.image</code> attribute. 
	 * @param value the image
	 */
	public void setImage(final Media value)
	{
		setImage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.image</code> attribute. 
	 * @param value the image
	 */
	public void setAllImage(final SessionContext ctx, final Map<Language,Media> value)
	{
		setAllLocalizedProperties(ctx,IMAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.image</code> attribute. 
	 * @param value the image
	 */
	public void setAllImage(final Map<Language,Media> value)
	{
		setAllImage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.imageUrl</code> attribute.
	 * @return the imageUrl
	 */
	public String getImageUrl(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedThejuGorantlaComponent.getImageUrl requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, IMAGEURL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.imageUrl</code> attribute.
	 * @return the imageUrl
	 */
	public String getImageUrl()
	{
		return getImageUrl( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.imageUrl</code> attribute. 
	 * @return the localized imageUrl
	 */
	public Map<Language,String> getAllImageUrl(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,IMAGEURL,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.imageUrl</code> attribute. 
	 * @return the localized imageUrl
	 */
	public Map<Language,String> getAllImageUrl()
	{
		return getAllImageUrl( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.imageUrl</code> attribute. 
	 * @param value the imageUrl
	 */
	public void setImageUrl(final SessionContext ctx, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedThejuGorantlaComponent.setImageUrl requires a session language", 0 );
		}
		setLocalizedProperty(ctx, IMAGEURL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.imageUrl</code> attribute. 
	 * @param value the imageUrl
	 */
	public void setImageUrl(final String value)
	{
		setImageUrl( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.imageUrl</code> attribute. 
	 * @param value the imageUrl
	 */
	public void setAllImageUrl(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,IMAGEURL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.imageUrl</code> attribute. 
	 * @param value the imageUrl
	 */
	public void setAllImageUrl(final Map<Language,String> value)
	{
		setAllImageUrl( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.linkTitle</code> attribute.
	 * @return the linkTitle
	 */
	public String getLinkTitle(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedThejuGorantlaComponent.getLinkTitle requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, LINKTITLE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.linkTitle</code> attribute.
	 * @return the linkTitle
	 */
	public String getLinkTitle()
	{
		return getLinkTitle( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.linkTitle</code> attribute. 
	 * @return the localized linkTitle
	 */
	public Map<Language,String> getAllLinkTitle(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,LINKTITLE,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.linkTitle</code> attribute. 
	 * @return the localized linkTitle
	 */
	public Map<Language,String> getAllLinkTitle()
	{
		return getAllLinkTitle( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.linkTitle</code> attribute. 
	 * @param value the linkTitle
	 */
	public void setLinkTitle(final SessionContext ctx, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedThejuGorantlaComponent.setLinkTitle requires a session language", 0 );
		}
		setLocalizedProperty(ctx, LINKTITLE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.linkTitle</code> attribute. 
	 * @param value the linkTitle
	 */
	public void setLinkTitle(final String value)
	{
		setLinkTitle( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.linkTitle</code> attribute. 
	 * @param value the linkTitle
	 */
	public void setAllLinkTitle(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,LINKTITLE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.linkTitle</code> attribute. 
	 * @param value the linkTitle
	 */
	public void setAllLinkTitle(final Map<Language,String> value)
	{
		setAllLinkTitle( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.linkUrl</code> attribute.
	 * @return the linkUrl
	 */
	public String getLinkUrl(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedThejuGorantlaComponent.getLinkUrl requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, LINKURL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.linkUrl</code> attribute.
	 * @return the linkUrl
	 */
	public String getLinkUrl()
	{
		return getLinkUrl( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.linkUrl</code> attribute. 
	 * @return the localized linkUrl
	 */
	public Map<Language,String> getAllLinkUrl(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,LINKURL,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ThejuGorantlaComponent.linkUrl</code> attribute. 
	 * @return the localized linkUrl
	 */
	public Map<Language,String> getAllLinkUrl()
	{
		return getAllLinkUrl( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.linkUrl</code> attribute. 
	 * @param value the linkUrl
	 */
	public void setLinkUrl(final SessionContext ctx, final String value)
	{
		if ( ctx == null) 
		{
			throw new JaloInvalidParameterException( "ctx is null", 0 );
		}
		if( ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedThejuGorantlaComponent.setLinkUrl requires a session language", 0 );
		}
		setLocalizedProperty(ctx, LINKURL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.linkUrl</code> attribute. 
	 * @param value the linkUrl
	 */
	public void setLinkUrl(final String value)
	{
		setLinkUrl( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.linkUrl</code> attribute. 
	 * @param value the linkUrl
	 */
	public void setAllLinkUrl(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,LINKURL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ThejuGorantlaComponent.linkUrl</code> attribute. 
	 * @param value the linkUrl
	 */
	public void setAllLinkUrl(final Map<Language,String> value)
	{
		setAllLinkUrl( getSession().getSessionContext(), value );
	}
	
}
