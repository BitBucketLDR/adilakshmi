/**
 *
 */
package com.uniquehire.dao.customer;

import java.util.List;

import com.hiring20.model.StudentModel;


/**
 * @author DELL
 *
 */
public interface CustomerDao
{
	List<StudentModel> getAllStudent();

	StudentModel getStudentById(String id);

	void register(StudentModel student);

}
