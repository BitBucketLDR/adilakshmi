/**
 *
 */
package com.uniquehire.dao.customer;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.hiring20.model.StudentModel;


/**
 * @author DELL
 *
 */
public class CustomerDaoImpl implements CustomerDao
{

	@Autowired
	private FlexibleSearchService flexibleSearchService;

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Autowired
	private ModelService modelService;


	/*
	 *
	 *
	 * @see com.uniquehire.dao.customer.CustomerDao#getAllStudent()
	 */
	@Override
	public List<StudentModel> getAllStudent()
	{

		final SearchResult<StudentModel> searchResult = flexibleSearchService.search("select {pk} from {student}");

		if (CollectionUtils.isNotEmpty(searchResult.getResult()))

		{
			return searchResult.getResult();
		}

		return null;
	}

	@Override
	public StudentModel getStudentById(final String id)
	{

		final Map<String, String> param = new HashMap<>();

		param.put("id", id);


		final SearchResult<StudentModel> searchResult = flexibleSearchService.search("select {pk} from {student} where {id} = ?id",
				param);

		if (CollectionUtils.isNotEmpty(searchResult.getResult()))

		{
			return searchResult.getResult().get(0);
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.dao.customer.CustomerDao#register(com.hiring20.model.StudentModel)
	 */
	@Override
	public void register(final StudentModel student)
	{

		modelService.save(student);

	}

}
