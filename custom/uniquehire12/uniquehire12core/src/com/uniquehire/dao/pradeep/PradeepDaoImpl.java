/**
 *
 */
package com.uniquehire.dao.pradeep;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.hiring20.model.StudentModel;


/**
 * @author DELL
 *
 */
public class PradeepDaoImpl implements PradeepDao
{

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.dao.pradeep.PradeepDao#getStudentDetails()
	 */
	@Autowired
	private FlexibleSearchService flexibleSearchService;
	@Autowired
	private ModelService modelService;

	@Override
	public List<StudentModel> getStudentDetails()
	{
		final SearchResult<StudentModel> result = flexibleSearchService.search("select {pk} from {student}");
		if (CollectionUtils.isNotEmpty(result.getResult()))
		{
			return result.getResult();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.dao.pradeep.PradeepDao#getStudentById(java.lang.String)
	 */
	@Override
	public StudentModel getStudentById(final String id)
	{
		final Map<String, String> param = new HashMap<>();
		final SearchResult<StudentModel> result = flexibleSearchService.search("select {pk} from {student} where id=?id", param);
		if (CollectionUtils.isNotEmpty(result.getResult()))
		{
			return result.getResult().get(0);
		}
		return null;
	}




	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.dao.pradeep.PradeepDao#register(com.hiring20.model.StudentModel)
	 */
	@Override
	public void register(final StudentModel student)
	{
		// YTODO Auto-generated method stub

	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}



}
