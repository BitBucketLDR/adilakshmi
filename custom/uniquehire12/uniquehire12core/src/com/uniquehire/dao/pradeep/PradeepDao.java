/**
 *
 */
package com.uniquehire.dao.pradeep;

import java.util.List;

import com.hiring20.model.StudentModel;


/**
 * @author DELL
 *
 */
public interface PradeepDao
{
	List<StudentModel> getStudentDetails();

	StudentModel getStudentById(String id);

	void register(StudentModel student);
}
