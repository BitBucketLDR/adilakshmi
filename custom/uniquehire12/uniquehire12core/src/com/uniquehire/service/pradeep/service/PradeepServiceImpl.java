/**
 *
 */
package com.uniquehire.service.pradeep.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hiring20.model.StudentModel;
import com.uniquehire.dao.pradeep.PradeepDao;


/**
 * @author DELL
 *
 */
public class PradeepServiceImpl implements PradeepService
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uniquehire.service.pradeep.service.PradeepService#getStudentDetails()
	 */
	@Autowired
	private PradeepDao pradeepDao;

	@Override
	public List<StudentModel> getStudentDetails()
	{

		return pradeepDao.getStudentDetails();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uniquehire.service.pradeep.service.PradeepService#getStudentById(java.lang.String)
	 */
	@Override
	public StudentModel getStudentById(final String id)
	{
		// YTODO Auto-generated method stub
		return pradeepDao.getStudentById(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uniquehire.service.pradeep.service.PradeepService#register(com.hiring20.model.StudentModel)
	 */
	@Override
	public void register(final StudentModel student)
	{

		pradeepDao.register(student);
	}

	/**
	 * @return the pradeepDao
	 */
	public PradeepDao getPradeepDao()
	{
		return pradeepDao;
	}

	/**
	 * @param pradeepDao
	 *           the pradeepDao to set
	 */
	public void setPradeepDao(final PradeepDao pradeepDao)
	{
		this.pradeepDao = pradeepDao;
	}

}
