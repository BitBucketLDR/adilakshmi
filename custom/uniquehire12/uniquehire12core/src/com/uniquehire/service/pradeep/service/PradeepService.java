/**
 *
 */
package com.uniquehire.service.pradeep.service;

import java.util.List;

import com.hiring20.model.StudentModel;


/**
 * @author DELL
 *
 */
public interface PradeepService
{
	List<StudentModel> getStudentDetails();

	StudentModel getStudentById(String id);

	void register(StudentModel student);

}
