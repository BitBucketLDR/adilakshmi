/**
 *
 */
package com.uniquehire.service.customer;

import java.util.List;

import com.hiring20.model.StudentModel;


/**
 * @author DELL
 *
 */
public interface CustomerService
{
	List<StudentModel> getAllStudent();

	StudentModel getStudentById(String id);

	void register(StudentModel student);
}
