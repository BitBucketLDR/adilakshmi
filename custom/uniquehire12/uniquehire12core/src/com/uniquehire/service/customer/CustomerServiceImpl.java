/**
 *
 */
package com.uniquehire.service.customer;


import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hiring20.model.StudentModel;
import com.uniquehire.dao.customer.CustomerDao;
import com.uniquehire12.core.event.StudentRegistrationEvent;


/**
 * @author DELL
 *
 */
public class CustomerServiceImpl implements CustomerService
{

	@Autowired
	private CustomerDao customerDao;
	@Autowired
	private EventService eventService;
	@Autowired
	private BaseStoreService baseStoreService;
	@Autowired
	private BaseSiteService baseSiteService;
	@Autowired
	private CommonI18NService commonI18NService;


	@Override
	public List<StudentModel> getAllStudent()
	{
		// YTODO Auto-generated method stub
		return customerDao.getAllStudent();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.service.customer.CustomerService#getStudentById(java.lang.String)
	 */
	@Override
	public StudentModel getStudentById(final String id)
	{
		// YTODO Auto-generated method stub
		return customerDao.getStudentById(id);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.service.customer.CustomerService#register(com.hiring20.model.StudentModel)
	 */
	@Override
	public void register(final StudentModel student)
	{
		// YTODO Auto-generated method stub

		customerDao.register(student);

		final StudentRegistrationEvent studentRegistrationEvent = new StudentRegistrationEvent();

		studentRegistrationEvent.setBaseStore(getBaseStoreService().getCurrentBaseStore());
		studentRegistrationEvent.setSite(getBaseSiteService().getCurrentBaseSite());
		studentRegistrationEvent.setCurrency(getCommonI18NService().getCurrentCurrency());
		studentRegistrationEvent.setLanguage(getCommonI18NService().getCurrentLanguage());
		studentRegistrationEvent.setStudent(student);
		System.out.println("we are in the service layer classs");
		eventService.publishEvent(studentRegistrationEvent);


	}

	/**
	 * @return the customerDao
	 */
	public CustomerDao getCustomerDao()
	{
		return customerDao;
	}

	/**
	 * @param customerDao
	 *           the customerDao to set
	 */
	public void setCustomerDao(final CustomerDao customerDao)
	{
		this.customerDao = customerDao;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.service.customer.CustomerService#getAllStudent()
	 */
	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}

	/**
	 * @param eventService
	 *           the eventService to set
	 */
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}

	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

}
