/**
 *
 */
package com.uniquehire12.core.user.dao;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.daos.impl.DefaultUserDao;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author DELL
 *
 */
public class ApparelUserDaoImpl extends DefaultUserDao implements ApparelUserDao
{

	/*
	 * @see com.uniquehire12.core.user.dao.ApparelUserDao#isNewCustomer(de.hybris.platform.core.model.user.UserModel)
	 */
	@Override
	public Boolean isNewCustomer(final UserModel userModel)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery("SELECT {pk} FROM {Customer} WHERE {uid}= ?emailId");
		query.addQueryParameter("emailId", userModel.getUid());

		final SearchResult<CustomerModel> searchResults = getFlexibleSearchService().search(query);

		if (CollectionUtils.isNotEmpty(searchResults.getResult())
				&& CollectionUtils.isEmpty(searchResults.getResult().get(0).getOrders()))
		{
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

}
