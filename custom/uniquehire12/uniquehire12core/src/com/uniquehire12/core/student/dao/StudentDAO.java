/**
 *
 */
package com.uniquehire12.core.student.dao;

import java.util.List;

import com.hiring20.model.StudentModel;


/**
 * @author DELL
 *
 */
public interface StudentDAO
{
	public List<StudentModel> getAllStudentDetails();

	public StudentModel getStudentById(String studentId);

	public void doStudentRegistration(StudentModel studentModel);

}
