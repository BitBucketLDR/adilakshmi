/**
 *
 */
package com.uniquehire12.core.student.dao.impl;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.hiring20.model.StudentModel;
import com.uniquehire12.core.student.dao.StudentDAO;


public class StudentDAOImpl implements StudentDAO
{

	@Autowired
	private FlexibleSearchService flexibleSearchService;

	@Autowired
	private ModelService modelService;

	/*
	 * @see com.uniquehire12.core.student.dao.StudentDao#getAllStudentDetails()
	 */
	@Override
	public List<StudentModel> getAllStudentDetails()
	{
		final String query = "SELECT {pk} FROM {Student}";

		final SearchResult<StudentModel> searchResults = flexibleSearchService.search(query);

		for (final StudentModel studentModel : searchResults.getResult())
		{
			System.out.println("Student ID:" + studentModel.getId());
			System.out.println("Student NAME:" + studentModel.getName());


		}

		return searchResults.getResult();
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	@Override
	public StudentModel getStudentById(final String studentId)
	{
		final String query = "SELECT {pk} FROM {Student} WHERE {id} = ?id";

		final HashMap parameters = new HashMap();

		parameters.put("id", studentId);

		final SearchResult<StudentModel> searchResults = flexibleSearchService.search(query, parameters);

		if (CollectionUtils.isNotEmpty(searchResults.getResult()) && searchResults.getResult().size() > 0)
		{

			return searchResults.getResult().get(0);

		}
		return null;

	}

	/*
	 * @see com.uniquehire12.core.student.dao.StudentDAO#doStudentRegistration(com.hiring20.model.StudentModel)
	 */
	@Override
	public void doStudentRegistration(final StudentModel studentModel)
	{

		modelService.save(studentModel);
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/*
	 *
	 *
	 * @see com.uniquehire12.core.student.dao.StudentDAO#dostudentRegistration(com.hiring20.model.StudentModel)
	 */


}
