/**
 *
 */
package com.uniquehire12.core.student.service;

import java.util.List;

import com.hiring20.model.StudentModel;


/**
 * @author DELL
 *
 */
public interface StudentService
{
	public List<StudentModel> getAllStudentDetails();

	public StudentModel getStudentById(String studentId);

	public void doStudentRegistration(StudentModel studentModel);
}
