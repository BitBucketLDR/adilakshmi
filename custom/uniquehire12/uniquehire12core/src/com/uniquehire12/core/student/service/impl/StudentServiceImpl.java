/**
 *
 */
package com.uniquehire12.core.student.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hiring20.model.StudentModel;
import com.uniquehire12.core.student.dao.StudentDAO;
import com.uniquehire12.core.student.service.StudentService;




/**
 * @author DELL
 *
 */
public class StudentServiceImpl implements StudentService
{
	@Autowired
	private StudentDAO studentDao;

	/* @see com.uniquehire12.core.student.service.StudentService#getAllStudentDetails() */

	@Override
	public List<StudentModel> getAllStudentDetails()
	{
		return studentDao.getAllStudentDetails();
	}

	/* @see com.uniquehire12.core.student.service.StudentService#getStudentById(java.lang.String) */

	@Override
	public StudentModel getStudentById(final String studentId)
	{

		return studentDao.getStudentById(studentId);
	}

	/** @return the studentDao */

	public StudentDAO getStudentDao()
	{
		return studentDao;
	}

	/**
	 * @param studentDao
	 *           the studentDao to set
	 */
	public void setStudentDao(final StudentDAO studentDao)
	{
		this.studentDao = studentDao;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire12.core.student.service.StudentService#doStudentRegistration(com.hiring20.model.StudentModel)
	 */
	@Override
	public void doStudentRegistration(final StudentModel studentModel)
	{
		studentDao.doStudentRegistration(studentModel);
	}



}
