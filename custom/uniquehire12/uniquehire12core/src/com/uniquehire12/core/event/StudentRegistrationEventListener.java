package com.uniquehire12.core.event;

import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.uniquehire12.core.model.RegistrationProcessModel;


/**
 * Listener for customer registration events.
 */
public class StudentRegistrationEventListener extends AbstractAcceleratorSiteEventListener<StudentRegistrationEvent>
{

	@Autowired
	private ModelService modelService;
	@Autowired
	private BusinessProcessService businessProcessService;

	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	protected SiteChannel getSiteChannelForEvent(final StudentRegistrationEvent event)
	{
		final BaseSiteModel site = event.getSite();
		ServicesUtil.validateParameterNotNullStandardMessage("event.order.site", site);
		return site.getChannel();

	}


	@Override
	protected void onSiteEvent(final StudentRegistrationEvent event)
	{
		final RegistrationProcessModel registrationProcessModel = (RegistrationProcessModel) getBusinessProcessService()
				.createProcess("studentRegistrationEmailProcess-" + event.getStudent().getId() + "-" + System.currentTimeMillis(),
						"studentRegistrationEmailProcess");

		System.out.println("we are in the listener classs so its executed succefully");
		registrationProcessModel.setSite(event.getSite());
		registrationProcessModel.setCustomer(event.getCustomer());
		registrationProcessModel.setLanguage(event.getLanguage());
		registrationProcessModel.setCurrency(event.getCurrency());
		registrationProcessModel.setStore(event.getBaseStore());
		registrationProcessModel.setId(event.getStudent().getId());
		registrationProcessModel.setName(event.getStudent().getName());
		getModelService().save(registrationProcessModel);
		System.out.println("saved succcessfully");
		getBusinessProcessService().startProcess(registrationProcessModel);


	}


}
