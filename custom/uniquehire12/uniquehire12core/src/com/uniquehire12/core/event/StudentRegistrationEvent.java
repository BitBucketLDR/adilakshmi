/**
 *
 */
package com.uniquehire12.core.event;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.event.AbstractCommerceUserEvent;

import com.hiring20.model.StudentModel;


/**
 * @author DELL
 *
 */
public class StudentRegistrationEvent extends AbstractCommerceUserEvent<BaseSiteModel>
{

	private StudentModel student;

	/**
	 * @return the student
	 */
	public StudentModel getStudent()
	{
		return student;
	}

	/**
	 * @param student
	 *           the student to set
	 */
	public void setStudent(final StudentModel student)
	{
		this.student = student;
	}
}
