/**
 *
 */
package com.uniquehire12.core.attributehandler;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

import java.util.Calendar;
import java.util.Date;


/**
 * @author DELL
 *
 */
public class CustomerAgeDynamicAttributHandler implements DynamicAttributeHandler<Integer, CustomerModel>
{

	/*
	 * @see
	 * de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#get(de.hybris.platform.servicelayer.model.
	 * AbstractItemModel)
	 */
	public static int age = 0;

	@Override
	public Integer get(final CustomerModel model)
	{
		try
		{
			final Date date = model.getDOB();

			final Calendar cal = Calendar.getInstance();

			cal.setTime(date);

			final int year = cal.get(Calendar.YEAR);

			final int year1 = Calendar.getInstance().get(Calendar.YEAR);

			age = year1 - year;


		}
		catch (final Exception e)
		{

			e.printStackTrace();

		}

		return Integer.valueOf(age);
	}

	/*
	 * @see
	 * de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler#set(de.hybris.platform.servicelayer.model.
	 * AbstractItemModel, java.lang.Object)
	 */
	@Override
	public void set(final CustomerModel arg0, final Integer val)
	{

		if (val != null)
		{
			throw new UnsupportedOperationException();
		}

	}

}
