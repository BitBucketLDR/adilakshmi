/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uniquehire12.storefront.controllers.pages;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 * Controller for home page
 */
@Controller

@RequestMapping("/uniqueHire")
public class UniqueHirePageController extends AbstractPageController
{
	@RequestMapping(method = RequestMethod.GET)
	public String uhire(final Model model) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId("uniqueHirePage"));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId("uniqueHirePage"));
		//updatePageTitle(model, getContentPageForLabelOrId(null));

		return getViewForPage(model);
	}


}
