/**
 *
 */
package com.uniquehire12.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uniquehire.facade.pradeep.facad.PradeepFacad;
import com.uniquehire12.facades.student.data.StudentData;
import com.uniquehire12.storefront.student.validator.PradeepValidator;


/**
 * @author DELL
 *
 */
@Controller
@RequestMapping("pradeepController")
public class PradeepControllerPage extends AbstractPageController
{
	@Autowired
	private PradeepFacad pradeepFacad;
	@Autowired
	private PradeepValidator pradeepValidator;

	@RequestMapping("all")
	public String getStudentDetails(@ModelAttribute("student") final StudentData student, final Model model)
	{

		if (CollectionUtils.isNotEmpty(pradeepFacad.getStudentDetails()))
		{
			model.addAttribute("students", pradeepFacad.getStudentDetails());
			return "/pages/layout/pradeepController";
		}


		model.addAttribute("fail", "there is no students available");
		return "/pages/layout/pradeepController";

	}

	@RequestMapping("{id}")
	public String getStudentById(@ModelAttribute("student") final StudentData student, final Model model,
			@PathVariable final String id)
	{

		if (pradeepFacad.getStudentById(id) != null)
		{
			model.addAttribute("students", pradeepFacad.getStudentById(id));
			return "/pages/layout/pradeepController";
		}


		model.addAttribute("fail", "there is no students available");
		return "/pages/layout/pradeepController";
	}

	@RequestMapping("pradeepRegister")
	public String getStudentDetails(@Valid @ModelAttribute("student") final StudentData student, final Model model,
			final BindingResult results)
	{
		pradeepValidator.validate(student, results);
		if (results.hasErrors())
		{
			model.addAttribute(new StudentData());
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return "/pages/layout/pradeepController";


		}
		pradeepFacad.register(student);
		return "/pages/layout/pradeepController";

	}

	/**
	 * @return the pradeepFacad
	 */
	public PradeepFacad getPradeepFacad()
	{
		return pradeepFacad;
	}

	/**
	 * @param pradeepFacad
	 *           the pradeepFacad to set
	 */
	public void setPradeepFacad(final PradeepFacad pradeepFacad)
	{
		this.pradeepFacad = pradeepFacad;
	}

	/**
	 * @return the pradeepValidator
	 */
	public PradeepValidator getPradeepValidator()
	{
		return pradeepValidator;
	}

	/**
	 * @param pradeepValidator
	 *           the pradeepValidator to set
	 */
	public void setPradeepValidator(final PradeepValidator pradeepValidator)
	{
		this.pradeepValidator = pradeepValidator;
	}

}
