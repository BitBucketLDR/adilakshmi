package com.uniquehire12.storefront.student.controller;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uniquehire12.facades.student.StudentFacade;
import com.uniquehire12.facades.student.data.StudentData;
import com.uniquehire12.storefront.student.validator.StudentValidator;



/**
 * @author DELL
 *
 */


@Controller
@RequestMapping("student")
public class StudentController extends AbstractPageController
{
	private static final String FORM_GLOBAL_ERROR = "form.global.error";
	@Resource
	private StudentFacade studentFacade;

	@Resource
	private StudentValidator studentValidator;

	/**
	 * @return the studentValidator
	 */
	public StudentValidator getStudentValidator()
	{
		return studentValidator;
	}

	/**
	 * @param studentValidator
	 *           the studentValidator to set
	 */
	public void setStudentValidator(final StudentValidator studentValidator)
	{
		this.studentValidator = studentValidator;
	}

	@RequestMapping("all")

	public String getAllStudentDetails(@ModelAttribute("student") final StudentData student, final Model model)
	{
		final List<StudentData> students = studentFacade.getAllStudentDetails();

		for (final StudentData studentData : students)
		{
			System.out.println("Student NAME:" + studentData.getName());
			System.out.println("Student ID:" + studentData.getId());

		}
		model.addAttribute("students", students);

		return "pages/account/studentDetailsPage";

	}

	@RequestMapping("{studentId}")

	public String getStudentDetailsById(@ModelAttribute("student") final StudentData student, @PathVariable final String studentId,
			final Model model)
	{
		//		final StudentData studentData = studentFacade.getStudentDetailsById(studentId);

		if (studentFacade.getStudentDetailsById(studentId) != null)
		{

			model.addAttribute("students", studentFacade.getStudentDetailsById(studentId));

			return "pages/account/studentDetailsPage";
		}
		model.addAttribute("fail", "there are no students available");

		return "pages/account/studentDetailsPage";
	}


	@RequestMapping("register")
	public String registration(@Valid @ModelAttribute("student") final StudentData student, final Model model,
			final BindingResult result)
	{
		studentValidator.validate(student, result);

		if (result.hasErrors())
		{
			model.addAttribute("student", new StudentData());
			model.addAttribute("message", "failure");

			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);

			return "pages/account/studentDetailsPage";

		}

		studentFacade.doStudentRegistration(student);
		model.addAttribute("message", "success");

		return "/";
	}


	/*
	 * @RequestMapping(name = "/register", method = RequestMethod.POST) public String
	 * doStudentRegistration(@ModelAttribute final StudentRegistrationForm studentRegistrationForm, final Model model,
	 * final RedirectAttributes redirectAttributes, final BindingResult bindingResult) throws CMSItemNotFoundException {
	 *
	 * studentValidator.validate(studentRegistrationForm, bindingResult);
	 *
	 * if (bindingResult.hasErrors()) {
	 *
	 * model.addAttribute("studentRegistrationForm", studentRegistrationForm); model.addAttribute("action",
	 * "/student/register"); model.addAttribute("buttonName", "SUBMIT");
	 *
	 * GlobalMessages.addInfoMessage(model, "Please fill the form details"); storeCmsPageInModel(model,
	 * getContentPageForLabelOrId(STUDENT_REGISTRATION_PAGE)); setUpMetaDataForContentPage(model,
	 * getContentPageForLabelOrId(STUDENT_REGISTRATION_PAGE)); return getViewForPage(model);
	 *
	 *
	 * }
	 *
	 * final StudentData studentData = new StudentData(); studentData.setId(studentRegistrationForm.getEmail());
	 * studentData.setName(studentRegistrationForm.getFname() + " " + studentRegistrationForm.getLname());
	 *
	 * studentFacade.doStudentRegistration(studentData);
	 *
	 * GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
	 * "Thank you for Registering");
	 *
	 * return REDIRECT_PREFIX + "/"; }
	 */
}











