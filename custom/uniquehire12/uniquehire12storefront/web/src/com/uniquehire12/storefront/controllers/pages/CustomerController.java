/**
 *
 */
package com.uniquehire12.storefront.controllers.pages;



import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;

import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uniquehire.facade.customer.CustomerFacade1;
import com.uniquehire12.facades.student.data.StudentData;
import com.uniquehire12.storefront.student.validator.MyValidator;


/**
 * @author DELL
 *
 */
@Controller


@RequestMapping("customercontrollerpage")
public class CustomerController extends AbstractPageController
{

	@Autowired
	private CustomerFacade1 customerFacade1;

	@Autowired
	private MyValidator myValidator;

	@RequestMapping("all")
	public String getAllStudent(@ModelAttribute("student") final StudentData student, final Model model)
	{

		if (CollectionUtils.isNotEmpty(customerFacade1.getAllStudent()))
		{
			model.addAttribute("students", customerFacade1.getAllStudent());

			return "/pages/layout/customercontrollerpage";
		}

		model.addAttribute("fail", "there are no students available");

		return "/pages/layout/customercontrollerpage";
	}

	@RequestMapping("{id}")
	public String getStudentById(@ModelAttribute("student") final StudentData student, final Model model,
			@PathVariable final String id)
	{

		if (customerFacade1.getStudentById(id) != null)
		{
			model.addAttribute("studentid", customerFacade1.getStudentById(id));

			return "/pages/layout/customercontrollerpage";
		}

		model.addAttribute("fail", "there are no students available");

		return "/pages/layout/customercontrollerpage";
	}


	@RequestMapping("registration")
	public String getAllStudent(@Valid @ModelAttribute("student") final StudentData student, final Model model,
			final BindingResult results)
	{

		myValidator.validate(student, results);

		if (results.hasErrors())
		{

			model.addAttribute(new StudentData());
			GlobalMessages.addErrorMessage(model, "form.global.error");


			return "/pages/layout/customercontrollerpage";
		}


		customerFacade1.register(student);

		return "/pages/layout/customercontrollerpage";
	}

	/**
	 * @return the customerFacade1
	 */
	public CustomerFacade1 getCustomerFacade1()
	{
		return customerFacade1;
	}

	/**
	 * @param customerFacade1
	 *           the customerFacade1 to set
	 */
	public void setCustomerFacade1(final CustomerFacade1 customerFacade1)
	{
		this.customerFacade1 = customerFacade1;
	}

	/**
	 * @return the myValidator
	 */
	public MyValidator getMyValidator()
	{
		return myValidator;
	}

	/**
	 * @param myValidator
	 *           the myValidator to set
	 */
	public void setMyValidator(final MyValidator myValidator)
	{
		this.myValidator = myValidator;
	}



}
