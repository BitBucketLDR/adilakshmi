/**
 *
 */
package com.uniquehire12.storefront.student.form;

/**
 * @author DELL
 *
 */
public class StudentRegistrationForm
{
	private String id;
	private String fname;
	private String lname;
	private String email;

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @param id
	 *           the id to set
	 */
	public void setId(final String id)
	{
		this.id = id;
	}


	/**
	 * @return the fname
	 */
	public String getFname()
	{
		return fname;
	}

	/**
	 * @param fname
	 *           the fname to set
	 */
	public void setFname(final String fname)
	{
		this.fname = fname;
	}

	/**
	 * @return the lname
	 */
	public String getLname()
	{
		return lname;
	}

	/**
	 * @param lname
	 *           the lname to set
	 */
	public void setLname(final String lname)
	{
		this.lname = lname;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}


}
