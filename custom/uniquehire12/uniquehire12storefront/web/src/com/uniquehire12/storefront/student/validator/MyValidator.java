/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uniquehire12.storefront.student.validator;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.uniquehire12.facades.student.data.StudentData;


/**
 * Validates registration forms.
 */
@Component("myValidator")
public class MyValidator implements Validator
{
	//public static final Pattern EMAIL_REGEX = Pattern.compile("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b");

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return RegisterForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final StudentData registerForm = (StudentData) object;

		validateName(errors, registerForm.getId(), "id", "Please Enter ID");
		validateName(errors, registerForm.getName(), "name", "Please Enter NAME");
		//	validateName(errors, registerForm.getEmail(), "email", "Please Enter EMAIL");


	}

	protected void validateName(final Errors errors, final String name, final String propertyName, final String property)
	{
		if (StringUtils.isBlank(name))
		{
			errors.rejectValue(propertyName, property);
		}
		else if (StringUtils.length(name) > 255)
		{
			errors.rejectValue(propertyName, property);
		}
	}

	/*
	 * protected void validateEmail(final Errors errors, final String email) { if (StringUtils.isEmpty(email)) {
	 * errors.rejectValue("email", "register.email.invalid"); } else if (StringUtils.length(email) > 255 ||
	 * !validateEmailAddress(email)) { errors.rejectValue("email", "register.email.invalid"); } }
	 * 
	 * 
	 * 
	 * public boolean validateEmailAddress(final String email) { final Matcher matcher = EMAIL_REGEX.matcher(email);
	 * return matcher.matches(); }
	 */
}
