/**
 *
 */
package com.uniquehire12.storefront.student.validator;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.uniquehire12.storefront.student.form.Task1Form;


/**
 * @author DELL
 *
 */
@Component("uniquehire12RegistrationValidatorl")
public class Uniquehire12RegistrationValidator extends RegistrationValidator
{
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator#validate(java.lang.Object,
	 * org.springframework.validation.Errors)
	 */
	@Override
	public void validate(final Object object, final Errors errors)
	{
		final Task1Form task1Form = (Task1Form) object;
		final String titleCode = task1Form.getTitleCode();
		final String firstName = task1Form.getFirstName();
		final String lastName = task1Form.getLastName();
		final String email = task1Form.getEmail();
		final String pwd = task1Form.getPwd();
		final String checkPwd = task1Form.getCheckPwd();
		final String DOB = task1Form.getDOB();
		final String phoneNumber = task1Form.getPhoneNumber();

		validateTitleCode(errors, titleCode);
		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}

		validateEmail(errors, email);
		validatePassword(errors, pwd);
		comparePasswords(errors, pwd, checkPwd);

		validateDOB(DOB, errors);
		validatePhoneNumber(phoneNumber, errors);



	}

	/**
	 * @param dOB
	 * @param errors
	 */
	private void validateDOB(final String dOB, final Errors errors)
	{
		if (StringUtils.isEmpty(dOB))
		{
			errors.rejectValue("DOB", "register.DOB.invalid");

		}

	}

	/**
	 * @param phoneNumber
	 * @param errors
	 */
	private void validatePhoneNumber(final String phoneNumber, final Errors errors)
	{
		if (StringUtils.isEmpty(phoneNumber))
		{
			errors.rejectValue("phoneNumber", "register.phonenumber.invalid");
		}

	}


}
