/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uniquehire12.storefront.controllers.cms;

import de.hybris.platform.cms2.model.contents.components.UniqueHireComponentModel;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uniquehire12.storefront.controllers.ControllerConstants;


/**
 * Controller for CMS SimpleResponsiveBannerComponent
 */
@Controller("UniqueHireComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.UniqueHireComponent)
public class UniqueHireComponentController extends AbstractAcceleratorCMSComponentController<UniqueHireComponentModel>
{


	// @Resource(name = "mediaFacade") private MediaFacade mediaFacade;



	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController#fillModel(javax.
	 * servlet.http.HttpServletRequest, org.springframework.ui.Model,
	 * de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel)
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final UniqueHireComponentModel component)
	{
		// YTODO Auto-generated method stub
		//	final MediaData mediaData = mediaFacade.getMediaByCode(component.getImage().getCode());

		model.addAttribute("image", component.getImage());
		model.addAttribute("linkTitle", component.getLinkTitle());
		model.addAttribute("content", component.getContent());
		model.addAttribute("imageUrl", component.getImageUrl());
		model.addAttribute("linkUrl", component.getLinkUrl());




	}



}
