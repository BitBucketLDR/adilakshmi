<%@ page trimDirectiveWhitespaces="true"%>

   <div class="row no-margin">
        <div class="col-xs-12 col-md-6 no-space"> 
<a href="${imageUrl }">
	<img title="${image.altText}" alt="${image.altText}" src="${image.url}">
</a>
</div>           
</div>
<div class="col-xs-12">
${content}
</div>
<a href="${linkUrl}" >
	${linkTitle}
</a>