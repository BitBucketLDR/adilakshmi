<pre>

1.${basicData.code}
2.${basicData.name}
3.${basicData.url}
4.${basicData.description}
5.${basicData.purchasable}
6.${basicData.stock}
7.${basicData.futureStocks}
8.${basicData.availableForPickup}
9.${basicData.averageRating}
10.${basicData.numberOfReviews}
11.${basicData.summary}
12.${basicData.manufacturer}
13.${basicData.variantType}
14.${basicData.price}
15.${basicData.baseProduct}
16.${basicData.images}
17.${basicData.categories}
18.${basicData.reviews}
19.${basicData.classifications}
20.${basicData.potentialPromotions}
21.${basicData.variantOptions}
</pre>

<pre>

These are all the category attributes

1.${categoryData.code}
2.${categoryData.name}
3.${categoryData.url}
4.${categoryData.description}
5.${categoryData.purchasable}
6.${categoryData.stock}
7.${categoryData.futureStocks}
8.${categoryData.availableForPickup}
9.${categoryData.averageRating}
10.${categoryData.numberOfReviews}
11.${categoryData.summary}
12.${categoryData.manufacturer}
13.${categoryData.variantType}
14.${categoryData.price}
15.${categoryData.baseProduct}
16.${categoryData.images}
17.${categoryData.categories}
18.${categoryData.reviews}
19.${categoryData.classifications}
20.${categoryData.potentialPromotions}
21.${categoryData.variantOptions}
</pre>


<pre>

These are all the price attributes

1.${priceData.code}
2.${priceData.name}
3.${priceData.url}
4.${priceData.description}
5.${priceData.purchasable}
6.${priceData.stock}
7.${priceData.futureStocks}
8.${priceData.availableForPickup}
9.${priceData.averageRating}
10.${priceData.numberOfReviews}
11.${priceData.summary}
12.${priceData.manufacturer}
13.${priceData.variantType}
14.${priceData.price}
15.${priceData.baseProduct}
16.${priceData.images}
17.${priceData.categories}
18.${priceData.reviews}
19.${priceData.classifications}
20.${priceData.volumePrices}
21.${priceData.price.value}
</pre>

<pre>

These are all the gallery attributes

1.${galleryData.code}
2.${galleryData.name}
3.${galleryData.url}
4.${galleryData.description}
5.${galleryData.purchasable}
6.${galleryData.stock}
7.${galleryData.futureStocks}
8.${galleryData.availableForPickup}
9.${galleryData.averageRating}
10.${galleryData.numberOfReviews}
11.${galleryData.summary}
12.${galleryData.manufacturer}
13.${galleryData.variantType}
14.${galleryData.price}
15.${galleryData.baseProduct}
16.${galleryData.images}
17.${galleryData.categories}
18.${galleryData.reviews}
19.${galleryData.classifications}
20.${galleryData.potentialPromotions}
21.${galleryData.variantOptions}
</pre>
<pre>

These are all the myvalues attributes

1.${myvaluesData.code}
2.${myvaluesData.name}
4.${myvaluesData.description}

</pre>