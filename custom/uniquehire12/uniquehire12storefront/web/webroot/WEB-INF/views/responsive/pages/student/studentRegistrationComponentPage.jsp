<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>




<form:form method="post" commandName="studentRegistrationForm" action="${action}">

	<formElement:formInputBox idKey="register.fname"
		labelKey="register.fname" path="fname" inputCSS="form-control" mandatory="true" />
	
	<formElement:formInputBox idKey="register.lname"
		labelKey="register.lname" path="lname" inputCSS="form-control" mandatory="true" />
	
	<formElement:formInputBox idKey="register.email" 
		labelKey="register.email" path="id" inputCSS="form-control" mandatory="true" />
		
	<div class="form-actions clearfix">
		<ycommerce:testId code="register_Register_button">
			<button type="submit" class="btn btn-default btn-block">
				<spring:theme code="${buttonName}" />
			</button>
		</ycommerce:testId>
	</div>	
		
</form:form>


<form:form method="post" commandName="studentRegistrationForm" action="register">

	<form:input path="id"/>
	<form:errors  path="id"></form:errors>
	<form:input path="fname"/>
	<form:errors  path="fname"></form:errors>
	<form:input path="lname"/>
	<form:errors  path="lname"></form:errors>
	<form:input path="email"/>
	<form:errors  path="email"></form:errors>
	
	<input type="submit" value = "Register"/>
		
</form:form>





