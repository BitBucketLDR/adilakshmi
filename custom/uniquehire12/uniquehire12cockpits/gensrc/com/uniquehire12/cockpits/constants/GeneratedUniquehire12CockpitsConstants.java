/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 */
package com.uniquehire12.cockpits.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedUniquehire12CockpitsConstants
{
	public static final String EXTENSIONNAME = "uniquehire12cockpits";
	
	protected GeneratedUniquehire12CockpitsConstants()
	{
		// private constructor
	}
	
	
}
