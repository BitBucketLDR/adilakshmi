/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uniquehire12.facades.process.email.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.CustomerModel;

import com.uniquehire12.core.model.RegistrationProcessModel;
import com.uniquehire12.facades.student.data.StudentData;


public class StudentEmailContext extends AbstractEmailContext<RegistrationProcessModel>
{


	private StudentData studentData;


	@Override
	public void init(final RegistrationProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{

		super.init(businessProcessModel, emailPageModel);
		studentData = new StudentData();
		System.out.println("we are in the final process that is the context class");
		studentData.setId(businessProcessModel.getId());
		studentData.setName(businessProcessModel.getName());
		put(DISPLAY_NAME, businessProcessModel.getName());



	}


	@Override
	protected BaseSiteModel getSite(final RegistrationProcessModel businessProcessModel)
	{
		return businessProcessModel.getSite();
	}


	@Override
	protected CustomerModel getCustomer(final RegistrationProcessModel businessProcessModel)
	{

		final CustomerModel customerModel = new CustomerModel();
		customerModel.setUid("pagadalalokeshr@gmail.com");
		customerModel.setName(businessProcessModel.getName());
		return customerModel;
	}


	@Override
	protected LanguageModel getEmailLanguage(final RegistrationProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}


}
