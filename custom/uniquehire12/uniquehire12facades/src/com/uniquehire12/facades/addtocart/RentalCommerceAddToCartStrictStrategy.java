/**
 *
 */
package com.uniquehire12.facades.addtocart;

import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;


/**
 * @author DELL
 *
 */
public class RentalCommerceAddToCartStrictStrategy extends DefaultCommerceAddToCartStrategy
{
	@Override
	protected CartEntryModel addCartEntry(final CommerceCartParameter parameter, final long actualAllowedQuantityChange)
			throws CommerceCartModificationException
	{
		final CartEntryModel cartEntryModel = super.addCartEntry(parameter, actualAllowedQuantityChange);

		cartEntryModel.setRental(parameter.getRental());
		return cartEntryModel;
	}

}
