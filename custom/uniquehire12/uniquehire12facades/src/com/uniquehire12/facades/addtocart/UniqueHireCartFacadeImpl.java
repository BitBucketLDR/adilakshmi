/**
 *
 */
package com.uniquehire12.facades.addtocart;

import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;


/**
 * @author DELL
 *
 */
public class UniqueHireCartFacadeImpl extends DefaultCartFacade implements UniqueHireCartFacade
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.uniquehire12.facades.addtocart.UniqueHireCartFacade#rentalAddToCart(java.lang.String, long,
	 * java.lang.String)
	 */
	@Override
	public CartModificationData rentalAddToCart(final String code, final long quantity, final String rental)
			throws CommerceCartModificationException
	{
		final AddToCartParams params = new AddToCartParams();
		params.setProductCode(code);
		params.setQuantity(quantity);
		params.setRental(rental);

		return addToCart(params);
	}

}
