/**
 *
 */
package com.uniquehire12.facades.addtocart;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;


/**
 * @author DELL
 *
 */
public interface UniqueHireCartFacade extends CartFacade
{

	CartModificationData rentalAddToCart(final String code, final long quantity, final String rental)
			throws CommerceCartModificationException;
}
