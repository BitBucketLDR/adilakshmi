/**
 *
 */
package com.uniquehire12.facades.task1.populator;

import de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;

import org.apache.commons.lang.StringUtils;


/**
 * @author DELL
 *
 */
public class CustomerPopulatorTask1 extends CustomerPopulator
{
	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator#populate(de.hybris.platform.core.
	 * model.user.CustomerModel, de.hybris.platform.commercefacades.user.data.CustomerData)
	 */
	@Override
	public void populate(final CustomerModel source, final CustomerData target)
	{
		super.populate(source, target);
		if (StringUtils.isNotEmpty(source.getPhonenumber()))
		{
			target.setPhoneNumber(source.getPhonenumber());
		}
		if (StringUtils.isNotEmpty(source.getDateOfBirth()))
		{
			target.setDateofbirth(source.getDateOfBirth());
		}

	}
}
