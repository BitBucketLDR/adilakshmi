/**
 *
 */
package com.uniquehire12.facades.student.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.hiring20.model.StudentModel;
import com.uniquehire12.facades.student.data.StudentData;


/**
 * @author DELL
 *
 */
public class StudentReversePopulator implements Populator<StudentData, StudentModel>
{

	/*
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final StudentData source, final StudentModel target) throws ConversionException
	{
		target.setId(source.getId());
		target.setName(source.getName());

	}

}
