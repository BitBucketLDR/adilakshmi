/**
 *
 */
package com.uniquehire12.facades.student.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hiring20.model.StudentModel;
import com.uniquehire12.core.student.service.StudentService;
import com.uniquehire12.facades.student.StudentFacade;
import com.uniquehire12.facades.student.data.StudentData;


/**
 * @author DELL
 *
 */
public class StudentFacadeImpl implements StudentFacade
{
	@Autowired
	private StudentService studentService;

	@Autowired
	private Converter<StudentModel, StudentData> studentConverter;

	@Autowired
	private Converter<StudentData, StudentModel> studentReverseConverter;

	/*
	 * @see com.uniquehire12.facades.student.StudentFacade#getAllStudentDetails()
	 */
	@Override
	public List<StudentData> getAllStudentDetails()
	{
		return getStudentConverter().convertAll(getStudentService().getAllStudentDetails());
	}

	/*
	 * @see com.uniquehire12.facades.student.StudentFacade#getStudentById(java.lang.String)
	 */
	@Override
	public StudentData getStudentDetailsById(final String studentId)
	{

		return getStudentConverter().convert(getStudentService().getStudentById(studentId));
	}

	/**
	 * @return the studentService
	 */
	public StudentService getStudentService()
	{
		return studentService;
	}

	/**
	 * @param studentService
	 *           the studentService to set
	 */
	public void setStudentService(final StudentService studentService)
	{
		this.studentService = studentService;
	}

	/**
	 * @return the studentConverter
	 */
	public Converter<StudentModel, StudentData> getStudentConverter()
	{
		return studentConverter;
	}

	/**
	 * @param studentConverter
	 *           the studentConverter to set
	 */
	public void setStudentConverter(final Converter<StudentModel, StudentData> studentConverter)
	{
		this.studentConverter = studentConverter;
	}

	/*
	 * @see com.uniquehire12.facades.student.StudentFacade#dostudentRegistration(com.uniquehire12.facades.student.data.
	 * StudentData)
	 */

	/**
	 * @return the studentReverseConverter
	 */
	public Converter<StudentData, StudentModel> getStudentReverseConverter()
	{
		return studentReverseConverter;
	}

	/**
	 * @param studentReverseConverter
	 *           the studentReverseConverter to set
	 */
	public void setStudentReverseConverter(final Converter<StudentData, StudentModel> studentReverseConverter)
	{
		this.studentReverseConverter = studentReverseConverter;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire12.facades.student.StudentFacade#doStudentRegistration(com.uniquehire12.facades.student.data.
	 * StudentData)
	 */
	@Override
	public void doStudentRegistration(final StudentData studentData)
	{
		studentService.doStudentRegistration(studentReverseConverter.convert(studentData));
	}

}
