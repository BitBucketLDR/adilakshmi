/**
 *
 */
package com.uniquehire12.facades.student;

import java.util.List;

import com.uniquehire12.facades.student.data.StudentData;


/**
 * @author DELL
 *
 */
public interface StudentFacade
{

	public List<StudentData> getAllStudentDetails();

	public StudentData getStudentDetailsById(String studentId);

	public void doStudentRegistration(StudentData studentData);
}
