/**
 *
 */
package com.uniquehire.facade.pradeep.facad;

import java.util.List;

import com.uniquehire12.facades.student.data.StudentData;


/**
 * @author DELL
 *
 */
public interface PradeepFacad
{
	List<StudentData> getStudentDetails();

	StudentData getStudentById(String id);

	void register(StudentData student);

}
