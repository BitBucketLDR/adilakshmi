/**
 *
 */
package com.uniquehire.facade.pradeep.facad;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hiring20.model.StudentModel;
import com.uniquehire.service.pradeep.service.PradeepService;
import com.uniquehire12.facades.student.data.StudentData;


/**
 * @author DELL
 *
 */
public class PradeepFacadImpl implements PradeepFacad
{

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.facade.pradeep.facad.PradeepFacad#getStudentDetails()
	 */

	@Autowired
	private Converter<StudentData, StudentModel> pradeepRegConverter;
	@Autowired
	private Converter<StudentModel, StudentData> pradeepConverter;
	@Autowired
	private PradeepService pradeepService;

	@Override
	public List<StudentData> getStudentDetails()
	{
		return pradeepConverter.convertAll(pradeepService.getStudentDetails());
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.facade.pradeep.facad.PradeepFacad#getStudentById(java.lang.String)
	 */
	@Override
	public StudentData getStudentById(final String id)
	{
		// YTODO Auto-generated method stub
		return pradeepConverter.convert(pradeepService.getStudentById(id));
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.facade.pradeep.facad.PradeepFacad#register(com.uniquehire12.facades.student.data.StudentData)
	 */
	@Override
	public void register(final StudentData student)
	{
		// YTODO Auto-generated method stub
		pradeepService.register(pradeepRegConverter.convert(student));

	}

	/**
	 * @return the pradeepService
	 */
	public PradeepService getPradeepService()
	{
		return pradeepService;
	}

	/**
	 * @param pradeepService
	 *           the pradeepService to set
	 */
	public void setPradeepService(final PradeepService pradeepService)
	{
		this.pradeepService = pradeepService;
	}

	/**
	 * @return the pradeepRegConverter
	 */
	public Converter<StudentData, StudentModel> getPradeepRegConverter()
	{
		return pradeepRegConverter;
	}

	/**
	 * @param pradeepRegConverter
	 *           the pradeepRegConverter to set
	 */
	public void setPradeepRegConverter(final Converter<StudentData, StudentModel> pradeepRegConverter)
	{
		this.pradeepRegConverter = pradeepRegConverter;
	}

	/**
	 * @return the pradeepConverter
	 */
	public Converter<StudentModel, StudentData> getPradeepConverter()
	{
		return pradeepConverter;
	}

	/**
	 * @param pradeepConverter
	 *           the pradeepConverter to set
	 */
	public void setPradeepConverter(final Converter<StudentModel, StudentData> pradeepConverter)
	{
		this.pradeepConverter = pradeepConverter;
	}

}
