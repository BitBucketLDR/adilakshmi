/**
 *
 */
package com.uniquehire.facade.customer;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.hiring20.model.StudentModel;
import com.uniquehire.service.customer.CustomerService;
import com.uniquehire12.facades.student.data.StudentData;


/**
 * @author DELL
 *
 */
public class CustomerFacadeImpl implements CustomerFacade1
{

	@Autowired
	private Converter<StudentData, StudentModel> customerRegConverter;
	@Autowired
	private Converter<StudentModel, StudentData> myConverter;
	@Autowired
	private CustomerService customerService;

	/**
	 * @return the customerRegConverter
	 */
	public Converter<StudentData, StudentModel> getCustomerRegConverter()
	{
		return customerRegConverter;
	}

	/**
	 * @param customerRegConverter
	 *           the customerRegConverter to set
	 */
	public void setCustomerRegConverter(final Converter<StudentData, StudentModel> customerRegConverter)
	{
		this.customerRegConverter = customerRegConverter;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.facade.customer.CustomerFacade#getAllStudent()
	 */
	@Override
	public List<StudentData> getAllStudent()
	{
		// YTODO Auto-generated method stub


		return myConverter.convertAll(customerService.getAllStudent());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.facade.customer.CustomerFacade#getStudentById(java.lang.String)
	 */
	@Override
	public StudentData getStudentById(final String id)
	{
		// YTODO Auto-generated method stub
		return myConverter.convert(customerService.getStudentById(id));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.uniquehire.facade.customer.CustomerFacade#register(com.uniquehire12.facades.student.data.StudentData)
	 */
	@Override
	public void register(final StudentData student)
	{
		// YTODO Auto-generated method stub

		customerService.register(customerRegConverter.convert(student));

	}

	/**
	 * @return the customerService
	 */
	public CustomerService getCustomerService()
	{
		return customerService;
	}

	/**
	 * @param customerService
	 *           the customerService to set
	 */
	public void setCustomerService(final CustomerService customerService)
	{
		this.customerService = customerService;
	}

	/**
	 * @return the myConverter
	 */
	public Converter<StudentModel, StudentData> getMyConverter()
	{
		return myConverter;
	}

	/**
	 * @param myConverter
	 *           the myConverter to set
	 */
	public void setMyConverter(final Converter<StudentModel, StudentData> myConverter)
	{
		this.myConverter = myConverter;
	}

}
