/**
 *
 */
package com.uniquehire.facade.customer;

import java.util.List;

import com.uniquehire12.facades.student.data.StudentData;


/**
 * @author DELL
 *
 */
public interface CustomerFacade1
{

	List<StudentData> getAllStudent();

	StudentData getStudentById(String id);

	void register(StudentData student);

}
