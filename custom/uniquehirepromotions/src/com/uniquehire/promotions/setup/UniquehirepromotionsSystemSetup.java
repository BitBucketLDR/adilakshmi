/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uniquehire.promotions.setup;

import static com.uniquehire.promotions.constants.UniquehirepromotionsConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.uniquehire.promotions.constants.UniquehirepromotionsConstants;
import com.uniquehire.promotions.service.UniquehirepromotionsService;


@SystemSetup(extension = UniquehirepromotionsConstants.EXTENSIONNAME)
public class UniquehirepromotionsSystemSetup
{
	private final UniquehirepromotionsService uniquehirepromotionsService;

	public UniquehirepromotionsSystemSetup(final UniquehirepromotionsService uniquehirepromotionsService)
	{
		this.uniquehirepromotionsService = uniquehirepromotionsService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		uniquehirepromotionsService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return UniquehirepromotionsSystemSetup.class.getResourceAsStream("/uniquehirepromotions/sap-hybris-platform.png");
	}
}
