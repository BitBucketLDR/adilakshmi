/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 */
package com.uniquehire.promotions.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedUniquehirepromotionsConstants
{
	public static final String EXTENSIONNAME = "uniquehirepromotions";
	
	protected GeneratedUniquehirepromotionsConstants()
	{
		// private constructor
	}
	
	
}
