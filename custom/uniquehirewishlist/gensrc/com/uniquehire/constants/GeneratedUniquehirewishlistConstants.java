/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 7, 2019 10:03:20 AM                     ---
 * ----------------------------------------------------------------
 */
package com.uniquehire.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedUniquehirewishlistConstants
{
	public static final String EXTENSIONNAME = "uniquehirewishlist";
	public static class TC
	{
		public static final String WISHLIST = "wishList".intern();
	}
	
	protected GeneratedUniquehirewishlistConstants()
	{
		// private constructor
	}
	
	
}
