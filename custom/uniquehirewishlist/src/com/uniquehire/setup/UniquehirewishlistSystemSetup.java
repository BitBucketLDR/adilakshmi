/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.uniquehire.setup;

import static com.uniquehire.constants.UniquehirewishlistConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.uniquehire.constants.UniquehirewishlistConstants;
import com.uniquehire.service.UniquehirewishlistService;


@SystemSetup(extension = UniquehirewishlistConstants.EXTENSIONNAME)
public class UniquehirewishlistSystemSetup
{
	private final UniquehirewishlistService uniquehirewishlistService;

	public UniquehirewishlistSystemSetup(final UniquehirewishlistService uniquehirewishlistService)
	{
		this.uniquehirewishlistService = uniquehirewishlistService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		uniquehirewishlistService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return UniquehirewishlistSystemSetup.class.getResourceAsStream("/uniquehirewishlist/sap-hybris-platform.png");
	}
}
